
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101" ,"Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


function introduce(student){
	let {name, age, classes} = student
	let [class1, class2] = classes

	console.log(`Hi! I'm ${name}. I am ${age} years old.`)
	console.log(`I study the following courses ${class1} and ${class2}.`)
}

const getCube = (num) => num ** 3
let cube = getCube(3);
console.log(cube)

let numArr = [15,16,32,21,21,2]
numArr.forEach(arr = (num) => {console.log(num)})

let numsSquared = numArr.map(squared = (num) =>num ** 2)
console.log(numsSquared);

class Dog {
	constructor (name, breed, dogAge){
		this.name = name 
		this.breed = breed
		this.dogAge = dogAge * 7
	}
}


let dog1 = new Dog("Mavi", "Husky", 5)
let dog2 = new Dog("MMs", "Golden Retriever", 10)

console.log(dog1)
console.log(dog2)
